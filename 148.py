#!/usr/bin/python

import sys

if len(sys.argv) != 5:
    print("Wrong number of parameters piped to %s." % sys.argv[0])
    print("Please try again with four integers passed.")
    sys.exit(0)

n = int(sys.argv[1])
first = int(sys.argv[2])
second = int(sys.argv[3])
third = int(sys.argv[4])

for arg in [first, second, third]:
    if arg >= n:
        print("Combination value (%d) cannot be greater than total number of possible numbers (%d)" % (arg, n))
        sys.exit(0)

## the following lines were used to read in valued from input piped to the program
## for example:
##
##      echo "5 1 2 3" | python3 ./148.py
##
## instead of the argv[] approach above.
##
## When using the piped in values, we had to change the if/then checks to 
## convert the strings piped in to integers.
##
##
# line = sys.stdin.readline().rstrip('\n')
# 
# n, first, second, third = line.rsplit(sep=" ")
# 
# # print("%s, %s, %s, %s" % (n, first, second, third))

firstSpin = (2 * n) + first
secondSpin = n + first + n - second
if third == second:
    thirdSpin = n
elif third < second:
    thirdSpin = n - second - third
else:
    thirdSpin = third - second

fullSpinCount = firstSpin + secondSpin + thirdSpin
## print("%d + %d + %d = %d" % (firstSpin, secondSpin, thirdSpin, fullSpinCount))
print(fullSpinCount)
